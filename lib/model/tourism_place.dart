class TourismPlace {
  String name;
  String location;
  String imageAsset;
  String openDay;
  String openingHours;
  String price;
  String description;
  List gallery;

  TourismPlace({
    required this.name,
    required this.location,
    required this.imageAsset,
    required this.openDay,
    required this.openingHours,
    required this.price,
    required this.description,
    required this.gallery
 });
}
