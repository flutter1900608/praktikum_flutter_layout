
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:praktikum_flutter_layout/model/tourism_place.dart';
import 'package:praktikum_flutter_layout/screen/list_item.dart';
import 'package:praktikum_flutter_layout/provider/done_tourism_provider.dart';
import 'package:provider/provider.dart';

import 'DetailScreen.dart';



class TourismList extends StatefulWidget{
  const TourismList({Key? key}) : super(key:key);

  @override
  _TourismListState createState() => _TourismListState();
}

class _TourismListState extends State<TourismList>{
  final List<TourismPlace> tourismPlaceList = [
    TourismPlace(
      name: 'Surabaya Submarine Monument',
      location: 'Jl Pemuda',
      imageAsset: 'assets/images/submarine.jpg',
      openDay: 'Monday-Friday',
      openingHours: '08:00 - 17.00',
      price: 'Rp 10.000',
      description: 'The Submarine Monument, or abbreviated as Monkasel, is a submarine museum located in Embong Kaliasin, Genteng, Surabaya. Located in the city center, namely on Jalan Pemuda, right next to Plaza Surabaya, and there is an access gate to access the mall from inside the monument.',
      gallery: [
        'https://lh3.googleusercontent.com/p/AF1QipPWssG4CHxC3fPfLuIAfpU8zSrsmm0D8KQH7Spi=s1360-w1360-h1020',
        'https://lh3.googleusercontent.com/p/AF1QipMh_Z_JnaDIX328a4WVCKC1fs4-Iew7aWofu1LK=s1360-w1360-h1020',
        'https://lh3.googleusercontent.com/p/AF1QipM8Djqw_JtGMG3PStFvf1lNJGKmCRJGV4Ipmp4i=s1360-w1360-h1020',
        'https://lh3.googleusercontent.com/p/AF1QipMTY4YOZdfOFgap0Agx_ctCtm4LP8A-85kRHij2=s1360-w1360-h1020',
      ],
    ),
    TourismPlace(
      name: 'Kelenteng Sanggar Agung',
      location: 'Kenjeran',
      imageAsset: 'assets/images/klenteng.jpg',
      openDay: 'every day',
      openingHours: '07:00 - 20.00',
      price: 'Rp 8.000',
      description: 'Sanggar Agung Temple or Hong San Tang Temple is a temple in the city of Surabaya. The address is at Jalan Sukolilo Number 100, Pantai Ria Kenjeran, Surabaya. This temple, apart from being a place of worship for adherents of the Tridharma, is also a tourist destination for tourists.',
      gallery: [
        'https://lh3.googleusercontent.com/p/AF1QipOsK46YBnsMkaR-UFQtyZt4BhszsnnnhytvPyY7=s1360-w1360-h1020',
        'https://lh3.googleusercontent.com/p/AF1QipN5yiUTHq2ZWHMSdgDzTx6CARwkVDs8WOQgnwbY=s1360-w1360-h1020',
        'https://lh3.googleusercontent.com/p/AF1QipMP6Fw_fQQoVyfmazS88ywKDJikFunWDdmYNGE=s1360-w1360-h1020',
        'https://lh3.googleusercontent.com/p/AF1QipMG1Zde_FD8y5SwfaSV8ng-wKwnP3tyiZ6rGwT6=s1360-w1360-h1020',
      ],
    ),
    TourismPlace(
      name: 'House of Sampoerna',
      location: 'Krembangan Utara',
      imageAsset: 'assets/images/house_sampoerna.jpg',
      openDay: 'Monday - Saturday',
      openingHours: '08:00 - 16.00',
      price: 'Rp 6.000',
      description: 'House of Sampoerna is a tobacco museum and Sampoernas headquarters located in Surabaya. The architectural style of the main building which is influenced by the Dutch colonial style was built in 1862 and is now a historical site.',
      gallery: [
        'https://lh3.googleusercontent.com/p/AF1QipMsgJRuQXCGTAY5_PNFbvZsZy30OeYxM-88eS0I=s1360-w1360-h1020',
        'https://lh3.googleusercontent.com/p/AF1QipOJ0BpYxUxjFwphft6kKhkNCqMoZW-PIZuRDM_8=s1360-w1360-h1020',
        'https://lh3.googleusercontent.com/p/AF1QipNuzYghmIzcOhzdn5ZEC44UVjoLU03ZG-WNmTQF=s1360-w1360-h1020',
        'https://lh3.googleusercontent.com/p/AF1QipP-T6RCEKT6UBRTX0z-pOVSvcOwq6uwfzkniWuT=s1360-w1360-h1020',
      ],
    ),
    TourismPlace(
      name: 'Tugu Pahlawan',
      location: 'Alun-alun contong',
      imageAsset: 'assets/images/pahlawan.jpg',
      openDay: 'Monday - Saturday',
      openingHours: '08:00 - 15.00',
      price: 'Rp 7.000',
      description: 'The Surabaya Ten November Museum is one of the museums located in the city of Surabaya, built in 1945. This museum was built with the aim of studying and deepening the events of the 1945 Ten November Battle. The Ten November Museum is located at Jalan Pahlawan, Surabaya.',
      gallery: [
        'https://lh3.googleusercontent.com/p/AF1QipPJe1zsqVx-5Nw5NfgmxPi_Wz_7-LphCcoqfAGU=s1360-w1360-h1020',
        'https://lh3.googleusercontent.com/p/AF1QipOFA9maLRAgiclD6UAiodnOgclm5o1Ukf7UgPYh=s1360-w1360-h1020',
        'https://lh3.googleusercontent.com/p/AF1QipPU41YwzkPm4XtKNkuFpR9emyXw43YjcufQmdBY=s1360-w1360-h1020',
        'https://lh3.googleusercontent.com/p/AF1QipOfKyMc2L8mSXzpi33eIiXiNPfBygsM3ovb_gBh=s1360-w1360-h1020',
      ],
    ),
    TourismPlace(
      name: 'Patung Suro Boyo',
      location: 'Wonokromo',
      imageAsset: 'assets/images/patung_sura.jpg',
      openDay: 'Every Day',
      openingHours: '24 hours',
      price: 'Rp 6.000',
      description: 'The statue of Sura and Baya is a statue which is a symbol of the city of Surabaya. This statue is in front of the Surabaya Zoo. This statue consists of two animals, namely a crocodile and a shark, which inspired the name of the city of Surabaya: ikan sura and baya.',
      gallery: [
        'https://lh3.googleusercontent.com/p/AF1QipP1tSoEYH8kDO6DmfR8-q02QWDR8IA2WGjXWVNb=s1360-w1360-h1020',
        'https://lh3.googleusercontent.com/p/AF1QipNqoRg-CDRj2MgVFAX3tnARq7D-fvD6ldeJStmy=s1360-w1360-h1020',
        'https://lh3.googleusercontent.com/p/AF1QipMuf3pr7EpKW5no8xlqHo-6QL9i8dbYkPBXks0G=s1360-w1360-h1020',
        'https://lh3.googleusercontent.com/p/AF1QipM1-I83mreKDdDARD-VvyDUuVRyXw8QclhytO7-=s1360-w1360-h1020',
      ],


    ),
  ];



  @override
  Widget build(BuildContext context) {
    final List<TourismPlace> doneTourismPlaceList =
        Provider.of<DoneTourismProvider>(
          context,
          listen: false,
        ).doneTourismPlaceList;

    return ListView.builder(
        itemBuilder: ( context, index) {
          final TourismPlace place = tourismPlaceList[index];
          return InkWell(
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return DetailScreen(place: place);
              }));
            },
            child: Consumer<DoneTourismProvider>(
              builder: (context, DoneTourismProvider data, widget){
                return ListItem(
                  place: place,
                  isDone: doneTourismPlaceList.contains(place),
                  onCheckboxClick: (bool? value){
                    setState(() {
                      if(value!=null) {
                        value
                            ? doneTourismPlaceList.add(place)
                            : doneTourismPlaceList.remove(place);
                        data.complete(place, value);
                      }
                    });
                  },
                );
                },
              ),
          );
        },
        itemCount: tourismPlaceList.length,
      );
  }
}