import 'package:flutter/material.dart';
import 'package:praktikum_flutter_layout/model/tourism_place.dart';


class DetailScreen extends StatelessWidget {
  const DetailScreen({Key? key, required this.place}) : super(key: key);

  final TourismPlace place;

  @override
  Widget build(BuildContext context) {
    List<Widget> galleryWidgetList = place.gallery.map(
        (imageLink) {
          return Padding(
            padding: const EdgeInsets.all(4.0),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(30.0),
              child: Image.network(imageLink),
            ),
          );
        },
    ).toList();
    print(galleryWidgetList);

    return Scaffold(
        body: SafeArea(
          child:Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Image.asset(place.imageAsset),
              Container(
                margin: const EdgeInsets.only(top: 16.0),
                child:  Text(
                  place.name,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 30.0,
                    fontFamily: 'Lobster',
                    //fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Container(
                margin: const EdgeInsets.symmetric(vertical: 16.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround, //spaceEvenly
                  children: <Widget>[
                    Column(
                      children:  <Widget>[
                        Icon(Icons.calendar_today),
                        Text(place.openDay),
                      ],
                    ),
                    Column(
                      children:  <Widget>[
                        Icon(Icons.watch_later_outlined),
                        Text(place.openingHours),
                      ],
                    ),
                    Column(
                      children:  <Widget>[
                        Icon(Icons.money_off),
                        Text(place.price),
                      ],
                    )
                  ],
                ),
              ), //icon
              Container(
                padding: const EdgeInsets.all(16.0),
                child: Text(
                 place.description,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 16.0,
                      fontFamily: 'Oxygen',
                  ),
                ),
              ), // container for description
              Container(
                height: 150,
                child: ListView(
                  scrollDirection: Axis.horizontal,
                  children: galleryWidgetList,
                  // children: <Widget>[
                  //   Padding(
                  //     padding:const EdgeInsets.all(4.0),
                  //     child: ClipRRect(
                  //       borderRadius: BorderRadius.circular(16.0),
                  //       child: Image.network(
                  //           'https://media-cdn.tripadvisor.com/media/photo-m/1280/16/a9/33/43/liburan-di-farmhouse.jpg'),
                  //     ),
                  //   ),
                  //   Padding(
                  //     padding: const EdgeInsets.all(4.0),
                  //     child: ClipRRect(
                  //       borderRadius: BorderRadius.circular(16.0),
                  //         child: Image.asset('assets/images/monkasel_1.jpg')),
                  //   ),
                  //   Padding(
                  //     padding: const EdgeInsets.all(4.0),
                  //     child: ClipRRect(
                  //         borderRadius: BorderRadius.circular(16.0),
                  //         child: Image.asset('assets/images/monkasel_2.jpg')),
                  //   ),
                  //   Padding(
                  //     padding: const EdgeInsets.all(4.0),
                  //     child: ClipRRect(
                  //         borderRadius: BorderRadius.circular(16.0),
                  //         child: Image.asset('assets/images/monkasel_3.jpg')),
                  //   )
                  //
                  // ],
                ),
              ),
            ],
          ),
        )
    );
  }
}